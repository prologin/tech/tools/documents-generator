# Documents

## Utilisation

### Setup de l'environnement

Si vous avez Nix avec les flakes activées, vous pouvez `nix develop` et vous
aurez toutes les dépendances nécessaires installées. Si vous avez Direnv,
`direnv allow`.

Sinon, si vous avez Docker, vous pouvez :

```bash
git clone git@gitlab.com:prologin/tech/tools/documents-generator.git
cd documents
docker run --rm -it -v $PWD:/app --entrypoint "" registry.gitlab.com/prologin/tech/tools/documents-generator:latest bash
```

Sinon, vous pouvez vous inspirer du fichier `Dockerfile` pour configurer votre
environnement. Bon courage.

### Générer un document

Les modèles de documents sont disponibles dans [asso/documents](https://gitlab.com/prologin/asso/documents).

1. Choisissez le document que vous voulez générer. Pour cet exemple, nous
   prendrons `note_de_frais`.
2. Éditez le fichier `values.yml` dans le dossier correspondant au document. En
   l’occurrence, `note_de_frais/values.yml`.
3. Lancez `./documents.py generate <document>`. En l’occurrence `./documents.py
   generate note_de_frais`.
4. Votre document se trouve dans `<document>/document.pdf`. En l’occurrence
   `note_de_frais/document.pdf`.

### Ajouter un document

Ça se passe dans [asso/documents](https://gitlab.com/prologin/asso/documents).

## Build

### Build de l'image Docker

Le Dockerfile fabrique une image qui package `document-generator` dans un
environnement avec Python et TeXLive full (nécessaire pour render le LaTeX).

Ce dernier a besoin qu'un dossier `texmf/` contenant les packages de
l'association soit disponible à la racine du projet.
La CI qui build l'image utilise le dossier `texmf/` du repo [asso/documents](https://gitlab.com/prologin/asso/documents).

## Good To Know

Ce repo (ou plutôt l'image Docker qui est générée) est notamment utilisée dans
la CI du repo [asso/documents](https://gitlab.com/prologin/asso/documents) pour
valider le format des documents.
