{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, poetry2nix, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (futils.lib) eachDefaultSystem defaultSystems;
      inherit (lib) recursiveUpdate;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ poetry2nix.overlay self.overlay ];
      });
    in
    recursiveUpdate
    {
      overlay = final: prev: {
        prologin = {
          texlive.prologin = final.callPackage ./texmf { };
        };
      };
    }
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            pandoc
            (texlive.combine {
              inherit (texlive) scheme-full;
              inherit (prologin.texlive) prologin;
            })

            poetry
            (pkgs.poetry2nix.mkPoetryEnv {
              projectDir = self;
            })

            terraform
          ];
        };
      }
    ));
}
