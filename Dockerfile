# syntax = docker/dockerfile:experimental

FROM python:3.10-slim as base

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    VENV_PATH="/opt/venv" \
    PATH="/opt/venv/bin:$PATH"

RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update && apt-get install -y --no-install-recommends \
    curl ca-certificates gnupg apt-transport-https

RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update && apt-get install -y --no-install-recommends \
    texlive texlive-full texlive-binaries

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python3-dev bison git

RUN --mount=type=bind,target=./pyproject.toml,src=./pyproject.toml \
    --mount=type=bind,target=./poetry.lock,src=./poetry.lock \
    --mount=type=cache,target=/root/.cache/pypoetry \
    python3 -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install poetry==1.7.1 && \
    poetry install

FROM base

ARG WORKDIR=/app
ARG UID=1000
ARG GID=1000

COPY --from=builder /opt/venv/ /opt/venv/

ENV TEXMFCNF=/app/.texmf-config/web2c:
RUN mkdir -p /app/.texmf-config/web2c && \
    echo "TEXMFLOCAL = /app/texmf" > /app/.texmf-config/web2c/texmf.cnf

COPY ./texmf /app/texmf
COPY ./documents.py /app/documents.py

RUN cd /app/texmf && texhash

RUN mkdir -p /app && \
    useradd -d /app -r -u ${UID} app && \
    chown app:${GID} -R /app && \
    mkdir -p "$WORKDIR"

USER app:${GID}
WORKDIR "$WORKDIR"

ENTRYPOINT ["python3", "/app/documents.py"]
