#! /usr/bin/env python3

import argparse
import inspect
import json
import logging
import os
import re
import subprocess
import sys
import tempfile
import uuid
from pathlib import Path
from typing import Iterable

import jinja2
import jsonschema
import yaml

logger = logging.getLogger()


class CustomJinjaEnvironment(jinja2.Environment):
    """
    A custom Jinja environment that automagically add functions prefixed with "filter_" as environment filters.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for name, func in inspect.getmembers(
            self, predicate=inspect.isfunction
        ):
            if name.startswith("filter_"):
                self.filters[name[7:]] = func


class LatexJinjaEnvironment(CustomJinjaEnvironment):
    LATEX_ESCAPE_CHARS = {
        "&": r"\&",
        "%": r"\%",
        "$": r"\$",
        "#": r"\#",
        "_": r"\_",
        "{": r"\{",
        "}": r"\}",
        "^": r"\^{}",
        "~": r"\textasciitilde{}",
        "\n": r"\\",
        "\\": r"\textbackslash{}",
        "<": r"\textless",
        ">": r"\textgreater",
    }

    # Combine all replacements into a single pattern
    LATEX_ESCAPE_REGEX = re.compile(
        "|".join(
            re.escape(key)
            for key in sorted(
                LATEX_ESCAPE_CHARS.keys(),
                key=len,
                reverse=True,
            )
        )
    )

    @staticmethod
    def filter_latexescape(value):
        return LatexJinjaEnvironment.LATEX_ESCAPE_REGEX.sub(
            lambda match: LatexJinjaEnvironment.LATEX_ESCAPE_CHARS[
                match.group()
            ],
            value,
        )

    @staticmethod
    def filter_preventempty(value):
        return value if value != "" else "~"  # unbreakable space


def validate_jsonschema(schema):
    """Validate the schema. Throws a ValueError if the schema is not valid."""
    validator = jsonschema.validators.validator_for(schema)

    try:
        validator.check_schema(schema)
    except jsonschema.exceptions.SchemaError as err:
        raise ValueError(str(err)) from err


def compute_values(values, default_values):
    result = default_values.copy()

    for k, _ in values.items():
        if isinstance(values.get(k), dict) and isinstance(
            default_values.get(k), dict
        ):
            result[k] = compute_values(values[k], default_values[k])
        else:
            result[k] = values[k]

    return result


def validate_values(schema, values):
    """Validate values against a schema. Throws a ValueError if the values are
    not valid. Expects the schema to be valid."""
    try:
        jsonschema.validate(values, schema)
    except jsonschema.exceptions.ValidationError as err:
        raise ValueError(str(err)) from err


def create_pdf(jobname, src):
    src = src.encode("utf-8")
    jobname = f"{jobname}-{uuid.uuid4()}"

    with tempfile.TemporaryDirectory() as td:
        args = [
            "pdflatex",
            f"-output-directory={td}",
            f"-jobname={jobname}",
            "-output-format=pdf",
        ]

        sp = subprocess.run(
            args,
            input=src,
            timeout=15,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=False,
        )

        try:
            with open(Path(td) / f"{jobname}.pdf", "rb") as f:
                pdf = f.read()
        except FileNotFoundError as e:
            raise RuntimeError(
                f"{sp.stdout.decode()}\n\n{sp.stderr.decode()}"
            ) from e

    return pdf


def get_document_schema(document_dir):
    with open(document_dir / "schema.json", "r", encoding="utf-8") as f:
        schema = json.load(f)

    return schema


def get_document_template(document_dir):
    with open(document_dir / "template.tex", "r", encoding="utf-8") as f:
        template = f.read()

    return template


def get_document_default_values(document_dir):
    if (document_dir / "default.yml").exists():
        with open(document_dir / "default.yml", "r", encoding="utf-8") as f:
            values = yaml.safe_load(f)
    else:
        values = {}

    return values


def get_document_values(document_dir):
    if (document_dir / "values.yml").exists():
        with open(document_dir / "values.yml", "r", encoding="utf-8") as f:
            values = yaml.safe_load(f)
    else:
        values = {}

    return values


def validate(paths: Iterable[Path], recursive: bool = False) -> None:
    if recursive:
        paths = [p for path in paths for p in path.rglob("*") if p.is_dir()]

    for p in paths:
        if not p.is_dir():
            logger.debug(f"Ignoring {p}")
            continue

        if not (p / "schema.json").exists():
            logger.debug(
                f"Ignoring {p} (does not contains a schema.json file)"
            )
            continue

        logger.info(f"Checking {p}")

        template = get_document_template(p)
        default_values = get_document_default_values(p)
        values = get_document_values(p)
        schema = get_document_schema(p)

        validate_jsonschema(schema)
        values = compute_values(values, default_values)
        validate_values(schema, values)


def render(document_dir, j2env=None, write_out=True):
    template = get_document_template(document_dir)
    default_values = get_document_default_values(document_dir)
    values = get_document_values(document_dir)

    values = compute_values(values, default_values)

    if j2env is None:
        j2env = LatexJinjaEnvironment()

    rendered = j2env.from_string(template).render(**values)

    if write_out:
        with open(document_dir / "rendered.tex", "w") as f:
            f.write(rendered)

    return rendered


def generate(document_dir):
    rendered = render(document_dir, write_out=False)
    file = create_pdf(document_dir.name, rendered)

    with open(document_dir / "document.pdf", "wb") as f:
        f.write(file)


def _get_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        title="commands", required=True, dest="command"
    )

    validate_cmd = subparsers.add_parser("validate")
    validate_cmd.add_argument(
        "-r",
        "--recursive",
        help="recursively validate all subdirectories of the specified paths",
        action="store_true",
        default=False,
    )
    validate_cmd.add_argument(
        "dir_paths",
        help="paths of the directories to validate",
        nargs="*",
        type=Path,
    )

    render_cmd = subparsers.add_parser("render")

    generate_cmd = subparsers.add_parser("generate")

    # `document` argument for `render` and `generate` subcommands
    for cmd in [render_cmd, generate_cmd]:
        cmd.add_argument(
            "document",
            help="path to the directory of the document to generate",
            type=Path,
        )

    return parser


def _setup_stdout_logger():
    logger.addHandler(logging.StreamHandler(sys.stdout))

    loglevel = os.environ.get("LOG_LEVEL", "INFO")

    if (
        debug := os.environ.get("DEBUG", None)
    ) is not None and debug.upper() in ("TRUE", "YES", "Y", "1"):
        loglevel = "DEBUG"

    logging._checkLevel(loglevel)
    logger.setLevel(getattr(logging, loglevel))


def _main():
    _setup_stdout_logger()

    args = _get_parser().parse_args()

    if args.command == "validate":
        paths = args.dir_paths if args.dir_paths else (Path.cwd(),)
        validate(paths, args.recursive)
    elif args.command == "render":
        render(args.document)
    elif args.command == "generate":
        generate(args.document)


if __name__ == "__main__":
    _main()
